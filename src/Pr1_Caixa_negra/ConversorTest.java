package Pr1_Caixa_negra;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class ConversorTest {

	@Test
	public void testEuro_a_Centimo() {
		double euros = 2;
		int CentimosEsperados = 200;
		int CentimosObtenidos = Conversor.DeEurosaCentimos(euros);
		assertEquals(CentimosEsperados, CentimosObtenidos);
	}

	@Test
	public void testCentimo_a_Euro() {
		int centimos = 100;
		double EurosEsperados = 1;
		double EurosObtenidos = Conversor.DeCentimosaEuros(centimos);
		assertEquals(EurosEsperados, EurosObtenidos);
	}
	
}
