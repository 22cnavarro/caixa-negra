package Pr1_Caixa_negra;

import java.util.Scanner;
import java.util.ArrayList;

public class Multiplicador {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> numero = new ArrayList<>();
		int num = -1;


		int multi = sc.nextInt();
		System.out.println(multiplica(numero, multi));
	}

	public static ArrayList<Integer> multiplica(ArrayList<Integer> numeros, int mult) {
		// crea una arraylist temporal
		ArrayList<Integer> temporal = new ArrayList<>();
		// con un for te recorres la lista
		for (int i = 0; i < numeros.size(); i++) {
			// multiplicas el valor de cada elemento y lo guardas en la arraylist temporal
			temporal.add(numeros.get(i) * mult);

		}
		return temporal;
	}

}
