package Pr1_Caixa_negra;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

class MultiplicadorTest {

	@Test
	void test() {
		ArrayList<Integer> numeros = new ArrayList<Integer>();
		numeros.add(34);
		numeros.add(24);
		numeros.add(12);
		numeros.add(01);
		
		ArrayList<Integer> resultadoEsperado = new ArrayList<Integer>();
		resultadoEsperado.add(68);
		resultadoEsperado.add(48);
		resultadoEsperado.add(24);
		resultadoEsperado.add(2);
		
		ArrayList<Integer> resultadoObtenido = Multiplicador.multiplica(numeros, 2);
		assertEquals(resultadoEsperado, resultadoObtenido);
	}

}