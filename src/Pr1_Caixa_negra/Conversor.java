package Pr1_Caixa_negra;

//Importar la clase Scanner para leer la entrada del usuario
import java.util.Scanner;

public class Conversor {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int centimos;
		Double euros;

		int opcion = 0;

		do {
			System.out.println(" teclea 1 para convertir euros a centimos teclea 2 para convertir centimos a euros");

			opcion = sc.nextInt();
			sc.nextLine();
			switch (opcion) {
			case 0:
				return;

			case 1:
				System.out.println("Ingresa la cantidad en euros");
				euros = sc.nextDouble();
				centimos = DeEurosaCentimos(euros);
				System.out.println(euros + " euros son " + centimos + " centimos");
				break;

			case 2:
				System.out.println("Ingresa la cantidad en centimos");
				centimos = sc.nextInt();
				euros = DeCentimosaEuros(centimos);
				System.out.println(centimos + " centimos son " + euros + " euros");
				break;

			default:
				break;
			}

		} while (opcion != 0);
	}

	public static int DeEurosaCentimos(double euros) {
		int centimos = (int) (euros * 100);
		return centimos;
	}

	public static double DeCentimosaEuros(int centimos) {
		double euros = (double) (centimos / 100);
		return euros;

	}

}