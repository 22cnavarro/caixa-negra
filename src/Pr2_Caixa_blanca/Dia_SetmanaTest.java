package Pr2_Caixa_blanca;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Dia_SetmanaTest {

	@Test
	void test() {
		//if (mes < 1 || mes > 12 || dia < 1 || dia > 31) {
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(32, 1, 2020));
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(32, 12, 2020));
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(0, 12, 2020));
		//pruebas
		assertEquals("Dissabte", Dia_Setmana.calcularDiaSetmana(1, 2, 2020));
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(1, 14, 2020));
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(35, 14, 2020));
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(0, 0, 0));
		assertEquals("Diumenge", Dia_Setmana.calcularDiaSetmana(2, 2, 2020));
		assertEquals("Dilluns", Dia_Setmana.calcularDiaSetmana(3, 2, 2020));
		assertEquals("Dimarts", Dia_Setmana.calcularDiaSetmana(4, 2, 2020));
		//} else if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
		assertEquals("Diumenge", Dia_Setmana.calcularDiaSetmana(5, 4, 2020));
		assertEquals("Divendres", Dia_Setmana.calcularDiaSetmana(5, 6, 2020));
		assertEquals("Dissabte", Dia_Setmana.calcularDiaSetmana(5, 9, 2020));
		assertEquals("Dijous", Dia_Setmana.calcularDiaSetmana(5, 11, 2020));
		
		assertEquals("Dijous", Dia_Setmana.calcularDiaSetmana(6, 2, 2020));
		assertEquals("Divendres", Dia_Setmana.calcularDiaSetmana(7, 2, 2020));
		assertEquals("Dissabte", Dia_Setmana.calcularDiaSetmana(8, 2, 2020));
		assertEquals("Diumenge", Dia_Setmana.calcularDiaSetmana(9, 2, 2020));
		assertEquals("Dimecres", Dia_Setmana.calcularDiaSetmana(9, 12, 2020));
		assertEquals("Dimarts", Dia_Setmana.calcularDiaSetmana(9, 2, 2021));
		assertEquals("Dimecres", Dia_Setmana.calcularDiaSetmana(9, 2, 2022));
		
		//if (dia > 29 || (dia == 29 && !esAnyTraspàs(any))) {
		assertEquals("Dilluns", Dia_Setmana.calcularDiaSetmana(28, 2, 2022));
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(29, 2, 2022));
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(31, 4, 2022));
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(30, 2, 2022));
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(30, 2, 2023));
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(29, 2, 2023));
		assertEquals("Dimecres", Dia_Setmana.calcularDiaSetmana(30, 3, 2022));
		
		//return "Error en el càlcul";
		assertEquals("Data incorrecta", Dia_Setmana.calcularDiaSetmana(-5, 4, 0));
		
	}

}