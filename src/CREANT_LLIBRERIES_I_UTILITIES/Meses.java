package CREANT_LLIBRERIES_I_UTILITIES;
/**
 * ENUM QUE GUARDA LOS 12 MESES DEL ANYO
 * @author CNR TRABAJO
 *
 */
public enum Meses {
	Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre
}