package CREANT_LLIBRERIES_I_UTILITIES;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class UtilConsole {
	/**
	 * FUNCIÓN QUE SIRVE PARA LLAMAR AL SCANNER
	 */
	static Scanner sc = new Scanner(System.in);
	// REGEX DNI
	public static final String DNI = "[0-9]{7,8}[A-Z a-z]";
	// REGEX EMAIL
	public static final String EMAIL = "(^.+)@(.+).(.+)$";
	// REGEX CP
	public static final String CP = "[0-9]{5}";
//REGEX TELEFON
	public static final String TELEFON = "[6-7][0-9]{8}";

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR STRINGS
	 * 
	 * @return
	 */
	public static String demanarString() {
		return sc.nextLine();
	}

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR INTS
	 * 
	 * @return
	 */
	public static int demanarInt() {
		return sc.nextInt();
	}

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR DOUBLES
	 * 
	 * @return
	 */
	public static Double demanarDouble() {
		return sc.nextDouble();
	}

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR BOOLEANS
	 * 
	 * @return
	 */
	public static Boolean demanarBoolean() {
		return sc.nextBoolean();
	}

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR DNIS
	 * 
	 * @return
	 */
	public static String demanarDNI() {
		String DNI = "";
		Boolean DNIok = false;
		do {
			DNI = demanarString();
			DNIok = validarDNI(DNI);
		} while (!DNIok);
		return DNI;

	}

	/**
	 * FUNCIÓN QUE SIRVE PARA VALIDAR DNIS
	 * 
	 * @param dni
	 * @return
	 */
	private static boolean validarDNI(String dni) {
		String lletres = "TRWAGMYFPDXBNJZSQVHLCKET";

		if (DNI.matches(DNI)) {
			int numDNI = Integer.parseInt(dni.substring(0, dni.length() - 1));
			char lletraDNI = dni.charAt(dni.length() - 1);
			int mod = numDNI % 23;
			if (lletres.charAt(mod) == lletraDNI)
				return true;
		}
		return false;
	}

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR GMAILS
	 * 
	 * @return
	 */
	public static String demanarEmail() {
		String Email = "";
		do {
			Email = demanarString();
			if (!Email.matches(EMAIL)) {
				System.out.println("Email incorrecto");
			}
		} while (!Email.matches(EMAIL));
		return Email;
	}

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR CODIGOS POSTALES
	 * 
	 * @return
	 */
	public static String demanarCP() {
		String CP = "";
		do {
			CP = demanarCP();
			if (!CP.matches(CP)) {
				System.out.println("CP incorrecto");
			}
		} while (!CP.matches(CP));
		return CP;
	}

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR TELEFONOS
	 * 
	 * @return
	 */
	public static String demanarTelefon() {
		String Telefon = "";
		do {
			Telefon = demanarString();
			if (!Telefon.matches(TELEFON)) {
				System.out.println("El telefono es incorrecto");
			}
		} while (!Telefon.matches(TELEFON));
		return Telefon;
	}

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR FECHAS
	 * 
	 * @return
	 */
	public static LocalDate demanarData() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yy");
		System.out.println("Data (dd-MM-yy)");
		String DataS = sc.next();
		LocalDate data = LocalDate.parse(DataS, dtf);
		System.out.println(data.format(dtf));
		return data;
	}

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR LAS RUTAS
	 * @return
	 */
	public static String demanarRuta() {
		String ruta = "";
		File f;
		do {
			ruta = sc.nextLine();
			f = new File("");

		} while (!f.exists());
		return ruta;
	}

	/**
	 * FUNCIÓN QUE SIRVE PARA PEDIR CONFIRMACIÓN
	 * @return
	 */
	public static Boolean demanarConfirmacio() {
		String Confirmar = "";
		boolean conf = false;
		do {
			String CONFIRMAR = sc.nextLine();
			if (Confirmar.equals("SI")) {
				conf = true;
			}
		} while (!Confirmar.equals("SI") && !Confirmar.equals("NO"));
		return conf;
	}

}