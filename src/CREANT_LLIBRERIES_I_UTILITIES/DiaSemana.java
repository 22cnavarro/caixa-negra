package CREANT_LLIBRERIES_I_UTILITIES;
/**
 * IMPORTANDO EL CALENDARIO DE LAS UTILIDADES DE JAVA
 */
import java.util.Calendar;
/**
 * ENUM QUE GUARDA LOS DIAS DE LA SEMANA
 * @author CNR TRABAJO
 *
 */
public enum DiaSemana {
	Lunes, Martes, Miercoles, Jueves, Viernes, Sabado, Domingo;

	/**
	 * FUNCIÓN QUE SIRVE PARA OBTENER EL DÍA DE LA SEMANA ACTUAL
	 * @return
	 */
	public static DiaSemana ObtenerDiaSemana() {
		Calendar calendario = Calendar.getInstance();
		int diaSemana = calendario.get(Calendar.DAY_OF_WEEK);
		return DiaSemana.values()[diaSemana - 1];

	}

}
