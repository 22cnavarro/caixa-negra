package CREANT_LLIBRERIES_I_UTILITIES;

import java.awt.Color;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class UtilRandom {
	/**
	 * REGEX QUE SIRVE PARA TELEFONO 
	 */
	public static final String TELEFON = "[6-7][0-9]{8}";

	/**
	 * Función que sirve para generar Random
	 */
	private static Random r = new Random();

	/**
	 * Función  que sirve para generar Números
	 * @return
	 */

	private static int generarNumero() {
		int num = r.nextInt();
		return num;
	}

	/**
	 * Función que sirve para generar Color Random
	 * @return
	 */
	private static Color generarcolor() {
		int red = r.nextInt(256);
		int green = r.nextInt(256);
		int blue = r.nextInt(256);
		return new Color(red, green, blue);
	}

	/**
	 * Función que sirve para generar Boleano Random
	 * @return
	 */
	private static boolean generarboolean() {
		return r.nextBoolean();
	}

	private static final int NUMERO_DE_DIGITOS = 9;

	private static String generarTelefon() {
		String telefon = "";
		do {
			for (int i = 0; i < NUMERO_DE_DIGITOS; i++) {
				int num = (int) (Math.random() * 10);
				telefon += num;
			}
			if (!telefon.matches(TELEFON)) {
				System.out.println("El telefono es incorrecto");
				telefon = "";
			}
		} while (telefon.isEmpty() || !telefon.matches(TELEFON));
		return telefon;
	}

	/**
	 * Función que sirve para generar una Fecha Random
	 * @return
	 */
	public static LocalDate generarFecha() {
		LocalDate fechaInicio = LocalDate.of(1970, 1, 1);
		// Generar un número aleatorio entre 0 y 365 * 50 (50 años)
		int diasAleatorios = r.nextInt(365 * 50);
		// Sumar los días aleatorios a la fecha de inicio
		LocalDate fechaAleatoria = fechaInicio.plusDays(diasAleatorios);
		return fechaAleatoria;
	}
/**
 * Función que sirve para generar un Usuario Random
 * @return
 */
	private static String generarUsuario() {
		final String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		final int longitudUsuario = 6;
		StringBuilder sb = new StringBuilder(longitudUsuario);
		for (int i = 0; i < longitudUsuario; i++) {
			int index = r.nextInt(caracteres.length());
			sb.append(caracteres.charAt(index));
		}
		return sb.toString();
	}

}